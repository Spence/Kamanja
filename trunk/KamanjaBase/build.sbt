name := "KamanjaBase"

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies += "log4j" % "log4j" % "1.2.17"

libraryDependencies += "org.json4s" %% "json4s-native" % "3.2.9" 

libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.2.9" 

libraryDependencies += "com.google.guava" % "guava" % "18.0" 

libraryDependencies += "com.google.code.findbugs" % "jsr305" % "1.3.9"
